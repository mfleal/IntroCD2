# Projeto: Impacto da Pandemia na Mobilidade das ocorrências criminais e atendimento nas UPAS em Curitiba

## Equipe: PandemiaMobilidadeCrimes

## Descrição: 
Identificar os impactos da Pandemia no Município de Curitiba
na relação espaço-tempo nas ocorrências de crimes (Furto e
Roubo, Tráfico e Consumo de Drogas) e pela busca de atendimento 
médico nos hospitais, Unidades Básicas de Saúde(UBS) e Unidades
de Pronto Atendimento (UPA).

## Membros:

1. Manoel Flavio Leal, 2122545, @mfleal, mfleal@gmail.com, PPGCA, UTFPR
2. Marcel Antunes Raposo, 131237, @marcelctb, marcelctb@gmail.com, PPGCA, UTFPR
3. William de Oliveira Souza, 1613073, @willdoliver, willdoliverml@gmail.com, PPGCA, UTFPR
