import csv
import pandas as pd
import glob, os


# @todo: Analisar 2019,2020 e 2021 separadamente
# @todo: Utilizar dados do heatmap para plotagens
# @todo: Trajectories of the center
#   https://userguide.mdanalysis.org/1.0.0/examples/transformations/center_protein_in_box.html


keep_cols = [
    'Data do Atendimento',
    'Data de Nascimento',
    'Sexo',
    # 'Código do Tipo de Unidade',
    # 'Tipo de Unidade',
    'Código da Unidade',
    # 'Descrição da Unidade',
    # 'Código do Procedimento',
    # 'Descrição do Procedimento',
    # 'Código do CID',
    # 'Descrição do CID',
    # 'Desencadeou Internamento',
    # 'Data do Internamento',
    # 'Estabelecimento Solicitante',
    # 'Estabelecimento Destino',
    # 'CID do Internamento',
    # 'Meio de Transporte',
    # 'Município',
    # 'Bairro',
    # 'Nacionalidade',
    # 'cod_usuario',
    # 'cod_profissional',
    'Unidade Atendimento',
    'Bairro Atendimento',
    'Bairro Moradia',
    # 'Data',
    'Ano',
    'Mes',
    'Dia',
    # 'Hora',
]

def formatFileData(filename):

    df = pd.read_csv(filename, sep=';', encoding ='latin1')
    df = df.rename(columns={'Bairro': 'Bairro Moradia'})

    # Filtering registers to Curitiba city
    df = df[df['Municício'] == 'CURITIBA']

    df['Unidade Atendimento'] = df['Descrição da Unidade']
    df['Unidade Atendimento'] = df['Unidade Atendimento'].str.replace('UPA', '')
    df['Unidade Atendimento'] = df['Unidade Atendimento'].str.replace('PSF', '')
    df['Unidade Atendimento'] = df['Unidade Atendimento'].str.replace('UMS', '')
    df['Unidade Atendimento'] = df['Unidade Atendimento'].str.strip()

    df['Bairro Moradia'] = df['Bairro Moradia'].str.replace('-', 'CENTRO')
    df['Bairro Moradia'] = df['Bairro Moradia'].str.replace('`CENTRO', 'CENTRO')
    df['Bairro Moradia'] = df['Bairro Moradia'].str.replace('0THOMAZ COELHO', 'THOMAZ COELHO')
    df['Bairro Moradia'] = df['Bairro Moradia'].str.replace('3JARDIM DONA BELIZARIA', 'JARDIM DONA BELIZARIA')

    df['Ano'] = df['Data do Atendimento'].str[6:10].astype(int)
    df['Mes'] = df['Data do Atendimento'].str[3:5].astype(int)
    df['Dia'] = df['Data do Atendimento'].str[:2].astype(int)

    # Filtering period of date
    df = df[
            ((df['Ano'] == 2019) | (df['Ano'] == 2020))
            & ((df['Mes'] > 3) | ((df['Mes'] == 3) & (df['Dia'] >= 16)))
    ]

    # de-para de unidades para bairros
    diffCities = pd.read_csv("e_saudeData/diffBairros_analysis_V1.csv", sep=";", encoding ="latin1")
    df = df.merge(diffCities, how='left', on='Unidade Atendimento')

    df['Bairro Moradia'] = df['Bairro Moradia'].str.strip()
    df['Bairro Atendimento'] = df['Bairro Atendimento'].str.strip()

    df = df[
            ((df['Bairro Moradia'] != 'NAO INFORMADO')
            & (df['Bairro Moradia'] != 'BAIRRO NAO INFORMADO')
            & (df['Bairro Moradia'] != 'OUTROS'))
            & ((df['Bairro Atendimento'] != 'NAO INFORMADO')
            & (df['Bairro Atendimento'] != 'BAIRRO NAO INFORMADO')
            & (df['Bairro Atendimento'] != 'OUTROS'))
    ]

    # print(df.head)
    # print(df.dtypes)
    # print(df.columns)

    new_df = df[keep_cols]
    # new_df.to_csv(filename+"_V1.csv", sep=';', index=False)
    # print(filename+" SAVED")

    return new_df

def mergeDataFrames(files):

    for file in files:
        print(file)

    print("Merging files")
    result = pd.DataFrame()
    for count, file in enumerate(files):
        print('working in file '+ str(count+1) + ' - ' + str(file))
        df = formatFileData(file)
        result = pd.concat([result, df])

    result.drop_duplicates(
        subset =['Data do Atendimento', 'Data de Nascimento', 'Sexo', 'Código da Unidade'],
        inplace = False)

    print(result.head)
    print(result.dtypes)
    print(result.columns)

    result.to_csv("e_saudeData/e_saude_medicos_merged.csv", sep=';', index=False)
    print("e_saudeData/e_saude_medicos_merged.csv SAVED" )

def diffBairros():
    df1 = pd.read_csv("e_saudeData/Bairro Atendimento_analysis_V1.csv", sep=';', encoding ='utf8')
    df2 = pd.read_csv("e_saudeData/Bairro Moradia_analysis_V1.csv", sep=';', encoding ='utf8')

    dfAtendimento = df1[["Bairro Atendimento"]].to_numpy()
    dfMoradia = df2[["Bairro Moradia"]].to_numpy()

    arrAtendimento = []
    for cityAt in dfAtendimento:
        cityAt = cityAt[0]
        arrAtendimento.append(cityAt)

    arrMoradia = []
    for cityAt in dfMoradia:
        cityAt = cityAt[0]
        arrMoradia.append(cityAt)

    print(arrAtendimento)
    print(arrMoradia)

    diffCity = list(set(arrAtendimento) - set(arrMoradia))
    diffCity.sort()
    print(pd.DataFrame(diffCity))
    
    pd.DataFrame(diffCity).to_csv("e_saudeData/diffBairros_analysis_V1.csv", sep=';', index=False)

if __name__ == "__main__":
    
    files = []
    for file in glob.glob("e_saudeData/Datasets/*.csv"):
        files.append(file)
    
    mergeDataFrames(files)

    